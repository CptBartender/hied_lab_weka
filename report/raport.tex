\documentclass[a4paper,11pt]{article}
\usepackage{latexsym}
\usepackage[polish]{babel}
\usepackage[utf8]{inputenc}
\usepackage[MeX]{polski}
\usepackage{hyperref}
\usepackage{natbib}
\usepackage{graphicx}

\author{Fryderyk Wysocki $\quad$ Karol Zandecki}

\title{Eksploracja danych \\ 
\large{\bf Weka -- Sprawozdanie I}} 

\begin{document}

\maketitle 

\section{Wstęp}
WEKA (Waikato Environment for Knowledge Analysis) jest biblioteką uczenia maszynowego służącą do eksploracji danych. Posiada ono wiele wbudowanych klasyfikatorów, filtrów oraz innych narzędzi udostępnianych w modelu Open Source, pozwalających w prosty i szybki sposób tworzyć własne rozwiązania dopasowane do analizowanego problemu.

Celem projektu było zapoznanie się z możliwościami oprogramowania WEKA poprzez implementację naiwnego klasyfikatora Bayesa oraz porównanie go z innymi dostępnymi algorytmami. 

Kod źródłowy algorytmu oraz programu testującego dostępny jest pod adresem: \newline\centerline{\url{https://bitbucket.org/RikerF/hied_lab_weka/src}}


\subsection{Opis algorytmów}
%TODO

\begin{description}
	\item[Naive Bayes] dostępny w bibliotece WEKA naiwnie (stąd nazwa) zakłada, że wszystkie cechy są niezależne. W ogólności prawdopodobieństwo wystąpienia obiektu o cechach $F_1..F_n$ przy klasie decyzyjnej C można zapisać jako $p(C, F_1,...,F_n)$, co można przekształcić w $p(C)*p(F_1|C)*p(F_2|C, F_1)*...*p(F_n|C,F_1,...F_{n-1})$. Wyliczenie tych prawdopodobieństw warunkowych może być niezwykle trudne w przypadku gdy mamy wiele cech lub gdy cechy przyjmują wiele różnych wartości. Klasyfikator ten zakłada jednak, że cecha ${F_i}$ jest niezależna od ${F_j}$ dla $i!=j$. W praktyce oznacza to, że przyjmujemy $p(F_i|C,F_j) = p(F_i|C)$, co upraszcza zapis: $p(C,F_1,...F_n) = p(C)*p(F_1|C)*...*p(F_n|C)$ \cite{Wikinaivebayes}.
	\item[Naive Bayes] zaimplementowany na podstawie opisu dostępnego na Wikipedii z niewielkim odstępem od głównych założeń algorytmu. W przypadku cech nominalnych za prawdopodobieństwo warunkowe $p(F_x|C)$ powinna być brana ilość wystąpień danej wartości cechy $F_x$ w klasie $C$ podzielona przez ilość wszystkich obiektów klasy $C$. Zdecydowano jednak by w przypadku nie wystąpienia cechy $F_x$ dla klasy $C$ w zbiorze uczącym, wyliczać prawdopodobieństwo warunkowe tak jakby cecha wystąpiła $0,5$ raza. Dzięki temu przypadki, które pozostałymi cechami wpasowywały się w daną klasę, nie były automatycznie przekreślane zerowym prawdopodobieństwem na jednej cesze. 
	\item[Decision Table] tworzy na podstawie zbioru uczącego tabelę decyzyjną zawierającą odpowiednio zapisane informacje o prawdopodobieństwie wystąpienia danej cechy w danej klasie. Następnie na jej podstawie przypisuje elementy zbioru testowego do odpowiednich klas decyzyjnych. \cite{Kohavi95thepower}
	
\end{description}

\section{Opis danych}
Do przeprowadzenia eksperymentu i porównania klasyfikatorów wybrany został zbiór ,,SPAM E-mail Database''\footnote{\url{http://repository.seasr.org/Datasets/UCI/arff/spambase.arff}} zawierający 4601 instancji danych opisywanych przez 58 cech. Baza zawiera informację o tym czy dany e-mail jest niechcianą wiadomością (klasa o wartościach nominalnych ,,SPAM'' i ,,NON-SPAM''), gdzie przypisania do klasy dokonano ręcznie. Rozkład elementów w poszczególnych klasach przedstawiony został w tabeli \ref{tab:dystrybucjaKlas}. Zbiór danych został wybrany z myślą o porównaniu algorytmów klasyfikacji w kontekście wykorzystania ich w filtrze antyspamowym o niewielkiej liczbie błędnych przypisań wiadomości do klas decyzyjnych. 

\begin{table}[tbph]
\begin{center}
\begin{tabular}{l|r|r}
Klasa & Liczba rekordów & Procent zbioru \\
\hline
SPAM & 1813 & 39,4\% \\
\hline
NON-SPAM & 2788 & 60,6\% \\
\end{tabular}
\end{center}
\caption{Dystrybucja klas}
\label{tab:dystrybucjaKlas}
\end{table}

\subsection{Atrybuty}
W analizowanym zbiorze danych występuje 58 atrybutów z czego 57 z nich posiada wartości liczbowe (w tym zmiennoprzecinkowe) a 1 jest klasą o wartościach nominalnych. Atrybuty podzielić można na 6 grup:

\begin{itemize}
	\item 48 atrybutów \textit{real} - procent występowania wybranego słowa w tekście wiadomości. Wartość dana jest wzorem: \begin{equation}\label{eq:procentSlow} A = 100 * \frac{N}{T} \end{equation} Gdzie $N$ oznacza liczbę wystąpień danego słowa a $T$ całkowitą liczbę słów w wiadomości.
	\item 6 atrybutów \textit{real} - procent występowania wybranego znaku w tekście wiadomości. Wartość obliczana jest analogicznie do wzoru \ref{eq:procentSlow}.
	\item 1 atrybut \textit{real} - średnia długość nieprzerwanych wystąpień dowolnego ciągu wielkich liter.
	\item 1 atrybut \textit{integer} - najdłuższe nieprzerwane wystąpienie dowolnego ciągu wielkich liter.
	\item 1 atrybut \textit{integer} - suma długości nieprzerwanych wystąpień dowolnego ciągu wielkich liter.
	\item 1 atrybut \textit{nominal} - klasa przypisująca wiadomość do spamu (wartość ,,1'') lub nie (,,0'').
\end{itemize}


\section{Wykorzystane miary}
Wybór miar służących do oceny klasyfikatorów podyktowany został chęcią weryfikacji skuteczności klasyfikatora jako filtru antyspamowego. Wykorzystane miary przedstawione zostały poniżej.

\begin{itemize}
	\item \textbf{Czas budowy} - czas potrzebny na stworzenie modelu klasyfikacji na podstawie danych uczących.
	\item \textbf{Czas klasyfikacji} - czas potrzebny na zastosowanie modelu klasyfikacji do danych testujących i przypisanie ich do klas decyzyjnych.
	\item \textbf{Współczynnik sukcesu} - procent poprawnie sklasyfikowanych danych.
	\item \textbf{Współczynnik ,,true positive''} - procent poprawnie sklasyfikowanych danych klasy ,,SPAM''. Wyraża się wzorem $TP=A/B$, gdzie $A$ - liczba poprawnie sklasyfikowanych danych wybranej klasy, $B$ - całkowita liczność klasy ,,SPAM''.
	\item \textbf{Współczynnik ,,false positive''} - procent niepoprawnie sklasyfikowanych danych klasy ,,NON-SPAM''. Wyraża się wzorem $TP=A/B$, gdzie $A$ - liczba niepoprawnie sklasyfikowanych danych wybranej klasy, $B$ - całkowita liczność klasy ,,NON-SPAM''. 
\end{itemize}

Czas budowy oraz klasyfikacji zostały wybrane ze względu na konieczną szybkość działania i operowanie na potencjalnie dużym zbiorze wiadomości e-mail. Natomiast dzięki dwu wartościowej klasie współczynniki "true positive" i "false positive" pozwalają w bardzo łatwy sposób oceniać poprawność działania algorytmu w kontekście filtru.

\section{Opis eksperymentu}
Eksperyment klasyfikacji przeprowadzony został dla wyżej opisanych danych z uwzględnieniem podziału na zbiór uczący oraz zbiór testujący w proporcjach 70\% do 30\%. Celem analizy było sprawdzenie w jaki sposób ziarno, wpływające na kolejność danych i przydział do jednego ze zbiorów, oddziałuje na działanie algorytmów. Dla każdego z klasyfikatorów zmierzone zostały czas budowy, czas klasyfikacji, współczynnik sukcesu, współczynniki ,,true positive'' oraz ,,false positive''. Ziarno zmieniane było w zakresie od 1 do 5 z krokiem 1.

\section{Wyniki}

Poniżej przedstawione zostały wyniki eksperymentu wraz z wyjaśnieniem. Dane na wykresach przedstawiane są w zależności od ustawionego ziarna (liczba od 1 do 5), pogrupowane według klasyfikatorów.

\subsection{Czas budowy i klasyfikacji}

W pierwszej kolejności analizie poddany zostanie czas budowy oraz klasyfikacji poszczególnych algorytmów. 

Jak pokazano na rysunku \ref{fig:buildTime} czas tworzenia klasyfikatora DecisionTable jest zdecydowanie najdłuższy (zgodnie z tabelą \ref{tab:srednieCzasy} około 85 razy większy od pozostałych). Związane jest to z koniecznością budowy tabeli decyzyjnej i liczenia prawdopodobieństw dla 58 cech oraz 3220 rekordów danych uczących.

\begin{table}[tbph]
\begin{center}
\begin{tabular}{l|r|r}
& Czas budowy [ms] & Czas klasyfikacji [ms] \\ 
\hline 
NaiveBayesSelfImpl & 38 & 16\\
\hline 
NaiveBayes & 39 & 29\\
\hline 
DecisionTable & 3409 & 10\\
\end{tabular} 
\end{center}
\caption{Porównanie średnich czasów budowy i klasyfikacji}
\label{tab:srednieCzasy}
\end{table}

Czas klasyfikacji DecisionTable jest lepszy od pozostałych ze względu na brak konieczności wykonywania dużej liczby obliczeń. Warto zauważyć, że zmienna wartość ziarna miała niewielkie znaczenie dla czasu klasyfikacji algorytmu NaiveBayesSelfImpl (własna implementacja NaiveBayes), co przedstawione zostało na rysunku \ref{fig:classifyTime} a średni czas klasyfikacji zaproponowanego algorytmu jest około 2 razy lepszy od NaiveBayes wbudowanego w WEKA.

\begin{figure}[htp]
\centering
\includegraphics[height=65mm]{images/buildTime.png}
\caption{Czas budowy klasyfikatora w zależności od ziarna\label{fig:buildTime}}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[height=65mm]{images/classifyTime.png}
\caption{Czas klasyfikacji danych w zależności od ziarna\label{fig:classifyTime}}
\end{figure}

\subsection{Współczynniki}
W dalszej części omówione zostaną wyniki dla współczynników sukcesu, ,,true positive'' oraz ,,false positive''. 

Pod względem poprawnej klasyfikacji zdecydowanym liderem jest DecisionTable, który zgodnie z tabelą \ref{tab:wspolczynniki} uzyskał średni wynik ponad 91\%. Jest to również najstabilniejszy algorytm odporny na zmianę ziarna i rozkładu danych w zbiorach. W przypadku NaiveBayesSelfImpl warto zauważyć, że dla pewnych wartości ziarna uzyskuje on znacznie gorsze wyniki (widoczne na rysunku \ref{fig:successRate}), co wyjaśnione zostanie w dalszej części. Jednakże anomalia ta ma wpływ na średni współczynnik sukcesu, przez co algorytm okazał się gorszy od dwóch pozostałych.

\begin{table}[tbph]
\begin{center}
\begin{tabular}{l|r|r|r}
& Sukces & True positive & False positive\\
\hline 
NaiveBayesSelfImpl & 72,99\% & 84,33\% & 43,42\% \\
\hline 
NaiveBayes & 79,36\% & 68,83\% & 4,87\% \\
\hline 
DecisionTable & 91,09\% & 95,29\% & 15,18\% \\
\end{tabular} 
\end{center}
\caption{Porównanie średnich wartości współczynników}
\label{tab:wspolczynniki}
\end{table}

\begin{figure}[htp]
\centering
\includegraphics[height=65mm]{images/successRate.png}
\caption{Współczynnik sukcesu w zależności od ziarna\label{fig:successRate}}
\end{figure}

Współczynniki ,,true positive'' oraz ''false positive'' informują o odpowiednio tym jaki procent danych został poprawnie i niepoprawnie zaklasyfikowany jako ,,SPAM''. Jak zauważyć można na rysunkach \ref{fig:truePositive} oraz \ref{fig:falsePositive} algorytm NaiveBayesSelfImpl dla pewnych wartości ziarna dokonuje klasyfikacji wszystkich danych do jednej klasy co czyni go bardzo podatnym na wybrany zbiór danych uczących. Natomiast dla pozostałych algorytmów wartość ziarna nie ma większego znaczenia i uzyskują one stabilne wartości współczynników.

Warto zauważyć, że DecisionTable posiada 3 razy większą wartość ,,false positive'' niż NaiveBayes. Oznacza to, że jest on zdecydowanie bardziej restrykcyjny i częściej przypisuje wiadomości do kategorii ,,SPAM''. Z kolei NaiveBayes jest bardziej dokładny i rzadziej kwalifikuje wiadomości jako niechciane.

\begin{figure}[htp]
\centering
\includegraphics[height=65mm]{images/truePositive.png}
\caption{Współczynnik ,,true positive'' w zależności od ziarna\label{fig:truePositive}}
\end{figure}
\begin{figure}[htp]
\centering
\includegraphics[height=65mm]{images/falsePositive.png}
\caption{Współczynnik ,,false positive'' w zależności od ziarna\label{fig:falsePositive}}
\end{figure}

\section{Podsumowanie}
Na podstawie wyników eksperymentu można stwierdzić, że najlepszym algorytmem do zastosowania jako filtr antyspamowy jest NaiveBayes wbudowany w WEKA. Posiada on najmniejszy współczynnik błędnych przypisań pożądanych wiadomości zapewniając przy tym dosyć wysoki współczynnik poprawnego wykrywania spamu. NaiveBayesSelfImpl rokuje duże nadzieje jeśli wyeliminowana zostanie zależność od danych uczących (występujące anomalie) oraz wprowadzone zostaną nieznaczne optymalizacje. Z kolei DecisionTable, mimo wysokiej wartości poprawnie przypisanych rekordów, zbyt dużą liczbą wiadomości klasyfikuje jako ,,SPAM'', przez co jego zastosowanie jako filtr może sprawiać wiele problemów użytkownikom.

\newpage

\bibliographystyle{plain}
\bibliography{bibliography.bib}

\end{document}  