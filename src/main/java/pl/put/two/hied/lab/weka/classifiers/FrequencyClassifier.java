package pl.put.two.hied.lab.weka.classifiers;

import java.util.Random;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

public class FrequencyClassifier extends Classifier {

	private static final long serialVersionUID = 8338893697890228957L;

	private Random random;
	private long randomSeed;

	private int[] count;
	private int total;
	private int classCount;

	public FrequencyClassifier(Random r) {
		randomSeed = r.nextLong();
	}

	@Override
	public void buildClassifier(Instances data) throws Exception {
		random = data.getRandomNumberGenerator(randomSeed);
		total = data.numInstances();
		classCount = data.numClasses();
		count = new int[classCount];

		for (int i = 0; i < total; i++) {
			count[(int) data.instance(i).classValue()]++;
		}
	}

	@Override
	public double classifyInstance(Instance instance) throws Exception {
		double result = 0;
		int x = random.nextInt(total);

		for (int i = 0; i < classCount; i++) {
			if (x < count[i]) {
				result = i;
				break;
			} else {
				x -= count[i];
			}
		}

		return result;
	}

	@Override
	public double[] distributionForInstance(Instance instance) throws Exception {
		// double[] dist = new double[classCount];
		// for(int i=0; i<classCount; i++)
		// dist[i]=(double)count[i]/(double)total;
		// return dist;
		return super.distributionForInstance(instance);
	}

}
