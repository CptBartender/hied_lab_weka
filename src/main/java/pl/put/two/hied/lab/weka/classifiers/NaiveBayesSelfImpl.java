package pl.put.two.hied.lab.weka.classifiers;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class NaiveBayesSelfImpl extends Classifier {

	private static final long serialVersionUID = -1284772896477144033L;

	private class ClassModel {
		public int myIndex;
		public int instMyClass = 0; // instances of my class
		public int instCount = 0; // total instances
		public ArrayList<HashMap<Object, Integer>> attrProbMap;
		public int attrCount = 0;
		public double[] avgValue;
		public double[] variance;
		public boolean[] attrIsNumeric;

		@SuppressWarnings("unused")
		public ClassModel() {
		};

		public ClassModel(int classIndex, Instances data, int iC) {
			myIndex = classIndex;
			instCount = iC;
			attrCount = data.numAttributes() - 1;
			avgValue = new double[attrCount];
			variance = new double[attrCount];
			attrIsNumeric = new boolean[attrCount];

			attrProbMap = new ArrayList<HashMap<Object, Integer>>();
			for (int i = 0; i < attrCount; i++) {
				attrProbMap.add(new HashMap<Object, Integer>());
			}

			@SuppressWarnings("unchecked")
			// This IS Enumeration<Attribute>
			Enumeration<Attribute> en = data.enumerateAttributes();
			int i = 0;
			while (en.hasMoreElements()) {
				attrIsNumeric[i] = en.nextElement().isNumeric();
				i++;
			}
		}

		public void insertIntoModel(Instance instance) {
			double[] attrValues = instance.toDoubleArray();
			HashMap<Object, Integer> hm;
			for (int i = 0; i < attrValues.length - 1; i++) {
				hm = attrProbMap.get(i);
				double d = attrValues[i];
				if (hm.containsKey(d))
					hm.put(d, hm.get(d) + 1);
				else
					hm.put(d, new Integer(1));
			}
			instMyClass++;
		}

		public void finalizeClassModel() {
			for (int i = 0; i < attrCount; i++) {
				if (!attrIsNumeric[i])
					continue;
				double meanSum = 0;

				HashMap<Object, Integer> hm = attrProbMap.get(i);
				for (Object o : hm.keySet()) {
					Double d = new Double(o.toString());
					meanSum += d * (double) hm.get(o);
				}
				avgValue[i] = meanSum / instMyClass;

				double varSum = 0;
				for (Object o : hm.keySet()) {
					Double d = new Double(o.toString());
					varSum += (d - avgValue[i]) * (d - avgValue[i]);
				}
				variance[i] = varSum / instMyClass;
			}
		}

		private double probabilityForAttr(double aVal, int aIndex) {
			if (attrIsNumeric[aIndex]) {
				double p1 = 1 / Math.sqrt(2 * Math.PI * variance[aIndex]);
				double d = aVal - avgValue[aIndex];
				d = d * d;
				double p2 = Math.exp(-d / (2 * variance[aIndex]));
				// System.out.println(p1*p2);
				return p1 * p2;
			} else {
				if (attrProbMap.get(aIndex).containsKey(aVal))
					return (double) attrProbMap.get(aIndex).get(aVal) / (double) instMyClass;
				else
					return 0.5 / instMyClass;
			}
		}

		public double probabilityForInstance(Instance instance) {
			double probability = (double) instMyClass / (double) instCount;
			double[] attrValues = instance.toDoubleArray();
			for (int i = 0; i < attrCount; i++)
				probability *= probabilityForAttr(attrValues[i], i);
			// System.out.println(probability);
			return probability;
		}

		@SuppressWarnings("unused")
		public void printModelSummary() {
			System.out.println(myIndex + " " + instMyClass + " " + instCount + " " + attrCount);
			for (int i = 0; i < attrCount; i++) {
				int countValues = 0;
				for (Object o : attrProbMap.get(i).keySet())
					countValues += attrProbMap.get(i).get(o);
				System.out.println("\t" + i + " " + attrProbMap.get(i).keySet().size() + " " + countValues);
			}

		}
	}

	private ArrayList<ClassModel> classModels;
	private int instanceCount;
	public int classCount;

	@Override
	public void buildClassifier(Instances data) throws Exception {
		instanceCount = data.numInstances();
		classCount = data.numClasses();
		classModels = new ArrayList<ClassModel>();

		for (int i = 0; i < classCount; i++) {
			classModels.add(new ClassModel(i, data, instanceCount));
		}

		for (int i = 0; i < data.numInstances(); i++) {
			Instance inst = data.instance(i);
			double classIndex = inst.classValue();
			classModels.get((int) classIndex).insertIntoModel(inst);
		}

		for (int i = 0; i < classCount; i++) {
			classModels.get(i).finalizeClassModel();
		}

		// for(int i=0; i<classCount; i++)
		// classModels.get(i).printModelSummary();
	}

	@Override
	public double classifyInstance(Instance instance) {
		int maxIndex = 0;
		double maxValue = Double.MIN_VALUE;
		for (int i = 0; i < classModels.size(); i++) {
			double value = classModels.get(i).probabilityForInstance(instance);

			if (value > maxValue) {
				maxValue = value;
				maxIndex = i;
			}
		}
		return maxIndex;
	}

}