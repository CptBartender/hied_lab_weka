package pl.put.two.hied.lab.weka;

import java.util.Random;

import pl.put.two.hied.lab.weka.classifiers.FrequencyClassifier;
import pl.put.two.hied.lab.weka.classifiers.NaiveBayesSelfImpl;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.rules.DecisionTable;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Experiment {

	private static final Data DATA = Data.SPAM;
	private static final double DATA_SPLIT = 0.7;
	private static final boolean PRINT_SUMMARY = false;

	private static int randomSeed = 0;
	private static Instances trainingSet;
	private static Instances testSet;

	private static void prepare() throws Exception {
		DataSource dataSource = new DataSource(DATA.getFilePath());
		Instances dataSet = dataSource.getDataSet(DATA.getClassColumn());
		int total = dataSet.numInstances();
		int div = (int) Math.round(total * DATA_SPLIT);

		dataSet.randomize(new Random(randomSeed));

		trainingSet = new Instances(dataSet, 0, div);
		testSet = new Instances(dataSet, div, total - div);
	}

	private static void launch(String name, Classifier classifier) throws Exception {
		Evaluation eval = new Evaluation(trainingSet);

		long start = System.nanoTime();
		classifier.buildClassifier(trainingSet);
		long stopBuild = System.nanoTime();
		eval.evaluateModel(classifier, testSet);
		long stop = System.nanoTime();

		System.out.println(name + "\t" + (stopBuild - start) / 1000000.0 + "\t" + (stop - stopBuild) / 1000000.0 + "\t"
				+ (1 - eval.errorRate()) + "\t" + eval.truePositiveRate(0) + "\t" + eval.falsePositiveRate(0) + "\t" + eval.trueNegativeRate(0) + "\t" + eval.falseNegativeRate(0));

		if (PRINT_SUMMARY) {
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toMatrixString());
		}
	}

	public static void main(String[] args) {
		try {
			System.out
					.println("Experimentt\tTime #buildClassifier [ms]\tTime #evaluateModel [ms]\tSuccess rate\tTP\tFP\tTN\tFN");
			for (int i = 0; i < 10; i++) {
				randomSeed = i;
				System.out.println("Seed\t" + i);
				prepare();
				launch("FrequencyClassifier", new FrequencyClassifier(new Random(randomSeed)));
				launch("NaiveBayesSelfImpl", new NaiveBayesSelfImpl());
				launch("NaiveBayes", new NaiveBayes());
				launch("DecisionTable", new DecisionTable());
			}
		} catch (Exception e) {
			System.out.println(e.toString());
			return;
		}
	}

	private enum Data {
		IRIS("data/iris.arff", 4), HSV("data/hsv.arff", 11), HABERMAN("data/haberman.arff", 3), BANK("data/bank.arff",
				10), SPAM("data/spam.arff", 57);

		private String filePath;
		private int classColumn;

		private Data(String filePath, int classColumn) {
			this.filePath = filePath;
			this.classColumn = classColumn;
		}

		public String getFilePath() {
			return filePath;
		}

		public int getClassColumn() {
			return classColumn;
		}
	}
}